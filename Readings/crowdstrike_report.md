## Glossary
1. Cyber Big Game Hunting (BGH): a type of cyberattack that usually leverages ransomware to target large, high-value organizations or high-profile entities. The practice of targeting large, financially well-off corporations in order to secure the biggest possible payouts. 
2. Single Extortion Ransomware: Encrypting the victim's files
3. Double Extortion Ransomware: Encrypt data + exfiltrating data & threatening to public release the victim's data unless the ransom is paid. Increase the pressure of the victim so that they can pay 
4. Triple Extortion Ransomware: Encrypt data + exfil data & threten to release data + DDoS attack. The victim has pressure where they experience a threat to critical operation.
5. Formjacking: cyber criminals inject malicious JavaScript code to hack a website and take over the funcitonality of the site's form page to collect sensitive user information. 

## Summary of the Crowdstrike report: 
1. There is an uptick in terms of using "double extortion model" as part of BGH. 
2. As adversaries are cloud concious, threat actors are relying on **valid cloud account** but also looking for **public-facing applications** as initial access.
3. TA also will be using **valid accounts that are higher-privileged** for priv escalation. 
4. In terms of defense evasion, they are moving away from deactivating of antivirus and firewall. They are more lean towards seeking way to modify authentication processes and attack identities. 
5. Third-party modules/library/packages will be weaponized by the attackers to exploit open-source projects. 
6. CrowdStrike Intelligence identified Ukrainian entity targeting in operations associated with various Russia state-nexus, Russia-aligned or likely Russia-origin adversaries throughout 2022. Consistent with Russia’s military focus, the Main Intelligence Directorate (GRU) seems to bear responsibility for many of the operations against Ukraine, although the Federal Security Service (FSB) has also supported the war effort through intelligence-collection activities. 

7. This campaign was highly likely intended to degrade the Ukrainian government’s ability to operate as well as psychologically impact Ukrainian citizens with the suggestion that Ukrainian authorities could not protect them from the ensuing military campaign.

8. Cyber security situational awareness refers to an understanding of the cyber threat environment within which it operates, its associated risk and impacts, and the adequacy of its risk mitigation measures.

9.	On January 14, 2022, prior to Russia’s invasion of Ukraine, a steady stream of intelligence collection activity performed against Ukrainian targets was supplemented by a series of disruptive and destructive EMBER BEAR operations that included website defacements and WhisperGate wiper malware deployments
10.	Psychological operations escalated during February 2022, with multiple DDoS attacks against Ukrainian government portals and financial institutions that likely aimed to exert pressure on Ukrainian citizens by disrupting their ability to conduct routine activities such as accessing banking services. 
11.	Many destructive Russian operations conducted against Ukrainian networks since the invasion began have been covertly run in efforts to deny Ukrainian citizens access to a specific resource — such as energy supply or a government database — without evoking public awareness.
12.	In contrast, destructive EMBER BEAR operations between January and February 2022 were conducted openly, defacing government websites to announce data destruction and public information leaks under the pretense of hacktivism to mislead attribution.
13. Russia-nexus offensive cyber operations continued at a highly elevated pace, although with a marked reduction in capability and tooling variety. 
14. This shift in quality suggests operations became more tactical and opportunistic at this time, likely reflecting a lack of planning beyond the Kremlin’s expectations of a short military conflict period.
15. These operations were highly likely more complex — though arguably with little wide-ranging effect — and therefore required longer staging and execution periods, illustrating the complexity of effectively leveraging cyber operations compared to well-established kinetic military doctrine.
16. Russian cyber activity during the second half of 2022 was largely characterized by a shift in focus to intelligence-collection operations, likely indicating increasing Russian military and Kremlin requirements for situational awareness as their advances into Ukraine stalled and reversed
17. This focused targeting likely indicates this adversary’s ambitions to gather intelligence related to Western military support to Ukraine, although the targeting of NGOs could also represent the preparation of information operations against organizations that may be involved in impending Russian war crime investigations.
18. While Russia’s cyber capabilities have undoubtedly contributed to Russia’s military campaign, they have also demonstrated inherent wartime limitations. This is particularly true in the case of destructive attacks, which frequently require extensive planning but are often less effective and enduring when compared to their kinetic counterparts.
19. These intrusions were likely intended to collect strategic intelligence, compromise intellectual property and further the surveillance of targeted groups, all of which are key Chinese Communist Party (CCP) intelligence goals.
20.	China-nexus adversaries primarily targeted organizations based in East Asia, Southeast Asia, Central Asia and South Asia that operated in the government, technology and telecommunications sectors.
21.	The scale and scope of China-nexus targeted intrusion activity is unlikely to contract in 2023, as cyber espionage remains a critical instrument to support the CCP’s strategic and economic ambitions.
22.	Formjacking will continue as a credible threat, allowing eCrime actors to steal, sell and/or make use of victim PII.