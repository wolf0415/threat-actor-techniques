// Q1
Email 
| where link contains "invasion.xyz"
| project sender
| distinct sender

// Q2 to Q3
Email 
| where link contains "invasion.xyz"
| count 

// Q6
OutboundBrowsing
| where url has "invasion.xyz"

// Q7,8,9
Employees
| join kind=inner OutboundBrowsing on $left.ip_addr == $right.src_ip
| where url has "invasion.xyz"
| where timestamp1 // there are multiple columns with the same name


// *Q10 
// Subject to write a more complex queries
FileCreationEvents
| where hostname == "VRDA-MACHINE" 
| where filename == "Flight-Crew-Information.xls"
// Resources: https://techcommunity.microsoft.com/t5/microsoft-defender-for-endpoint/hunting-tip-of-the-month-browser-downloads/ba-p/220454



// Q12
let regexT = @"[ \w-]+\.[ \w-]*$";
Email
| where sender == "tethys@pocketbook.xyz"
| extend extracted_filename_from_link = extract(regexT,0,link)
| summarize numBer_of_files=count() by extracted_filename_from_link
| render piechart 
// run separately here 
| project extracted_filename_from_link
| distinct extracted_filename_from_link
| count 

// Resources: https://learn.microsoft.com/en-us/azure/data-explorer/kusto/query/extractfunction
// https://learn.microsoft.com/en-us/azure/data-explorer/kusto/query/tutorials/use-aggregation-functions



// Q13 (talking about http domain)
Email 
| where recipient has "richard_clements"
| where sender has "tethys"
| project link

// present data that entered in a day 
ProcessEvents
| where timestamp between (datetime(2023-02-11T00:00:00) .. datetime(2023-02-11T23:59:00))

Employees
| where name == "Richard Clements"
| project hostname, ip_addr

Email
| where link contains "antennas-passers.com" and recipient == "richard_clements@iowaballoons.com"
| where filename == "Flight-Crew-Information.xls"
// When they downloaded the files
ProcessEvents
| where hostname == "HNOA-LAPTOP" and username == "riclements" 
| where timestamp between (datetime(2023-03-04)..datetime(2023-03-05))

// Q18 
let maliciuos_file = 
FileCreationEvents 
| where timestamp between (datetime(2023-03-04T07:50:00) .. datetime(2023-03-04T07:58:00))
| where hostname == "HNOA-LAPTOP"
| project path;
// How many processes were spawned on Richard Clement's machine by the file in (18)?
ProcessEvents
| where hostname == "HNOA-LAPTOP" and (process_name contains "yeargood.exe" or parent_process_name contains "yeargood.exe")
| summarize count()

//  Q22 - The file in (18) established a remote connection from Richard Clement's machine to an external IP over port 443. What was this IP?
let remote_connect_ip = 
ProcessEvents
| where hostname == "HNOA-LAPTOP" and (process_name contains "yeargood.exe" or parent_process_name contains "yeargood.exe")
| extend ip_addr_extracted = extract(@"\d{1,3}\.\d{1,3}\.\d{1,3}\.\d{1,3}",0,process_commandline) // ip regex
| where isnotempty(ip_addr_extracted) // remove columns that are empty from view
| project ip_addr_extracted; 
PassiveDns
| where ip == "118.3.14.33"
| where ip in (remote_connect_ip)

// Q24
let selectTimeStamp1 = datetime(2023-03-04T21:21:58);
let seDr = dynamic(["services.exe", "sc.exe"]);
ProcessEvents
| where hostname == "HNOA-LAPTOP"
| where timestamp > selectTimeStamp1
| where not(parent_process_name has_any (seDr)) // filter case-insensitive strings similar to in~
// the "~" represent filter with case-insensitive
| where process_commandline contains "mimikatz"

// Q25
let selectTimeStamp1 = datetime(2023-03-04T21:21:58);
let seDr = dynamic(["services.exe", "sc.exe"]); // create an array 
ProcessEvents
| where hostname == "HNOA-LAPTOP"
| where timestamp > selectTimeStamp1
| where not(parent_process_name has_any (seDr)) // not operator (not in something)
| where process_commandline contains ".txt"

// 26
InboundBrowsing
| where src_ip == "118.3.14.33"
| summarize total_pc_connected_to_C2 = count() by src_ip
| project total_pc_connected_to_C2, src_ip

ProcessEvents
| where process_commandline matches regex "rundll32.exe \\d{1,3}\\.\\d{1,3}\\.\\d{1,3}\\.\\d{1,3}"
| distinct username
| count
