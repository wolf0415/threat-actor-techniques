-- Question 2: How many employees are in the company?
Employees
| count 


-- Question 3: Each employee at Balloons Over Iowa is assigned an IP address. Which employee has the IP address: “192.168.2.191”?
Employees
| where ip_addr == "192.168.2.191"


-- Question 4: How many emails did Betty Land receive?

Email 
| where recipient has "betty_land" // if we are not sure what are the domains are
| count 

-- or 

Email
| where recipient == "betty_land@iowaballoons.com" 

-- List out how many of the organizations users sent email
Email 
| where sender has "iowaballoons.com" 
| distinct sender 


`contains` operator doesnt match the exact words while "has" match the exact

--  Question 5: How many Balloons Over Iowa employees received emails with the term “ufos” in the subject?
 Email 
| where recipient has "iowaballoons.com"
| where subject has "ufos"
| distinct recipient
| count 


-- Questions 6: How many unique websites did “Jorge Hardwick” visit?
Employees
| join kind=inner OutboundBrowsing on $left.ip_addr == $right.src_ip // merge two tables with different names
| where name == "Jorge Hardwick" 
| distinct url // for the purpose of sanity check
| count 

-- Q7:  How many domains in the PassiveDns records contain the word "infiltrate"? (hint: use the contains operator instead of has. If you get stuck, do a take 10 on the table to see what fields are available.) 
PassiveDns
| where domain contains "infiltrate" 
| distinct domain


-- Q8:  What IPs did the domain “cheeseburger-infiltrate.com” resolve to (enter any one of them)?
PassiveDns
| where domain contains "cheeseburger-infiltrate.com" 
| project ip, domain

-- Q9: How many unique URLs were browsed by employees named “Karen”?
let james_ips = 
Employees
| where name has "Karen"
| distinct ip_addr;
OutboundBrowsing
| where src_ip in (james_ips)
| distinct url 
| count ;


