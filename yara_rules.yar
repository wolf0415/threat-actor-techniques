rule Detector_file {
	meta: 
		description = "This rule is to detect cracker"
		source = ""
		status = ""
		sharing = ""
		category = ""
		malware = ""
		// unsure whether these are the keywords or what
		tc_detection_type = "" 
		tc_detection_name = ""
		tc_detection_factor = ""
		md5 = "5801ef4cf8e06345db3634af1efd68e9"

	strings: 
		// wide = search for strings encoded with UTF-16
		// ascii = match ASCII characters, only required if `wide` is used
		$powershell_usage = "powershell.exe" wide ascii 
		$url1 = "hxxps[://]g[it[hub-[rea[dme[.]com/up[da[te_SC[.]bat" wide ascii
		$url2 = "hxxps[://]x0[.]at/1[8v[6[.]bat" wide ascii
		$dll_file = "imageres.dll" wide ascii
	condition:
		($url2 or $url1) and $dll_file and $powershell_usage
}

