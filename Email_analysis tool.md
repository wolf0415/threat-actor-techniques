# Tools for phishing email analysis 
## Checklist of the info we need to collect from email header 
1. Sender email address
2. Sender IP address
3. Reverse lookup of the sender IP address
4. Email subject line
5. Recipient email address (this information might be in the CC/BCC field)
6. Reply-to email address (if any)
7. Date/time

### Specific headers that we need to inspect
1.	X-Originating-IP - The IP address of the email was sent from (this is known as an X-header)
2.	Smtp.mailfrom/header.from - The domain the email was sent from (these headers are within Authentication-Results)
3.	Reply-To - This is the email address a reply email will be sent to instead of the From email address
4. From: This displays who the message is from, however, this can be easily forged and can be the least reliable.
5. Subject: This is what the sender placed as a topic of the email content
6. Date: This shows the date and time the email message was composed.
7. To:  shows to whom the message was addressed but may not contain the recipient's address.
8. Return-Path: The email address for return mail. This is the same as "Reply-To:".
9. Envelop-To: This header shows that this email was delivered to the mailbox of a subscriber whose email address is user@example.com.
10. Delivery Date: This shows the date and time at which the email was received by your (mt) service or email client.
11. Received: The received is the most important part of the email header and is usually the most reliable. They form a list of all the servers/computers through which the message traveled in order to reach you. The received lines are best read from bottom to top. That is, the first "Received:" line is your own system or mail server. The last "Received:" line is where the mail originated. Each mail system has their own style of "Received:" line. A "Received:" line typically identifies the machine that received the mail and the machine from which the mail was received.
12. Dkim-Signature & Domainkey-Signature: These are related to domain keys which are currently not supported by (mt) Media Temple services. You can learn more about these by visiting: http://en.wikipedia.org/wiki/DomainKeys.
13. Message-id: A unique string assigned by the mail system when the message is first created. These can easily be forged.
14. Mime-Version: Multipurpose Internet Mail Extensions (MIME) is an Internet standard that extends the format of email. Please see http://en.wikipedia.org/wiki/MIME for more details.
15. Content-Type: Generally, this will tell you the format of the message, such as html or plaintext.
16. X-Spam-Status: Displays a spam score created by your service or mail client.
17. X-Spam-Level: Display s spam score usually by your service or mail client
18. Message Body: This is the actual content of the email itself, written by the sender. 

NOTE: The original sender can be known through `X-Originating-IP header` or `Received` headers

## Checklist of the artifacts we need to collect from the email body 
1. Any URL links (if an URL shortener service was used, then we’ll need to obtain the real URL link)
2. The name of the attachment
3. The hash value of the attachment (hash type MD5 or SHA256, preferably the latter)

## Email header analysis 
1. Messageheader: https://toolbox.googleapps.com/apps/messageheader/analyzeheader
2. Message Header Analyzer: https://mha.azurewebsites.net/
3. Mail header: https://mailheader.org/

## Tools that analyze information on sender's IP address
1. IPinfo.io: https://ipinfo.io/
2. URLScan.io: https://urlscan.io/
3. Capture an image of the website without us visiting: https://www.url2png.com/
4. https://www.wannabrowser.net/
5. Talos Reputation Center: https://talosintelligence.com/reputation

## Email body analysis
1. Extract URL from message header or text: https://www.convertcsv.com/url-extractor.htm

## File Reputation checker
1. Talos File Reputation: https://talosintelligence.com/talos_file_reputation
2. VirusTotal — https://www.virustotal.com/gui/home/upload

## Phishtool 
1. Reverse enginner what phishing email is doing: https://www.phishtool.com/

## What are DMARC, DKIM and SPF? 
1. These are email authentication protocols that could help prevent spammers or phishers from sending emails on the behalf of a domain they do not own. 
2. DKIM and SPF: as if compare to a business license or a doctor's medical degree displayed on the wall of an office — they help demonstrate legitimacy.
3. DMARC: tells mail servers what to do when DKIM or SPF fail, whether that is marking the failing emails as "spam," delivering the emails anyway, or dropping the emails altogether.

### How does Sender Policy Framework (SPF) work? 
1. A way for a domain to list all the servers they send emails from. Think of it like a publicly available employee directory that helps someone to confirm if an employee works for an organization.
2. SPF records list all IP addresses of all the servers that are allowed to send emails from the domain. 

https://www.cloudflare.com/learning/email-security/dmarc-dkim-spf/

https://www.proofpoint.com/au/threat-reference/spf

