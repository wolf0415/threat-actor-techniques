import requests 
import time 
import json

indicators = [] # put your list of hashes here

api_key = "" # insert your api key here

url = "https://www.virustotal.com/vtapi/v2/file/report"

for indicator in indicators: 
    params = { 'apikey': api_key, 'resource':indicator}
    response = requests.get(url,params=params)
    response_json = json.loads(response.content)

    if response_json['positives'] <= 0: 
        print(f'{indicator} => NOT MALICIOUS')
    elif 1 >= response_json['positives'] >= 3: 
        print(f'{indicator} => MAYBE MALICIOUS')
    elif response_json['positives'] >= 4: 
        print(f'{indicator} => MALICIOUS')
    else: 
        print("Hashes not found")
    time.sleep(20)
