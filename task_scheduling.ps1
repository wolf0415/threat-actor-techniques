# This is reference from the YouTube video on Task scheduling
# Link: https://www.youtube.com/watch?v=_88SIKfXrBI


New-ScheduledTaskSettingsSet # configure settings of a scheduled task 
New-ScheduledTaskTrisgger -AtLogOn # configure when to run the task 
New-ScheduledTaskTrigger -Once -At 7am -RepetitionInterval (New-TimeSpan -Minutes 30) -RandomDelay (New-TimeSpan -Minutes 10)
# -RandomDelay => not wanting to execute the something straight after 30 minutes 

New-ScheduledTaskPrincipal -GroupId "Builtin\Users" -RunLevel Highest # -RunLevel => will escalate privilege


$DoStuff = New-ScheduledTaskAction -Execute "powershell" -Argument '-File C:\Temp\AppxRemoval.ps1'
$TimeToDoStuff = New-ScheduledTaskTrigger -AtLogOn
$SettingsFortheStuff = New-ScheduledTaskSettingsSet -DisallowDemandStart

# build the scheduled task that has all the components needed 
$FinalBuildOfTheStuff = New-ScheduledTask -Action $DoStuff -Trigger $TimeToDoStuff -Settings $SettingsFortheStuff

# Officially run the set the scheduled task into the system
Register-ScheduledTask -TaskName "Remove Appx" -InputObject $FinalBuildOfTheStuff  -User ""
# -User => you can specify the user you want to execute as 

<# Change ExecutionPolicy #>
$DoStuff = New-ScheduledTaskAction -Execute 'powershell' -Argument '"Set-ItemProperty -Path HKCU:\....\Microsoft.Powershell -Name ExecutionPolicy -Value RemoteSigned'
$TimeToDoStuff = New-ScheduledTaskTrigger -AtLogOn
$SettingsFortheStuff = New-ScheduledTaskSettingsSet 
$whorunninit = New-ScheduledTaskPrincipal -GroupId "Builtin\Users" -RunLevel Highest

Register-ScheduledTask -TaskName "Change Execution Policy" -Action $DoStuff -Settings $SettingsFortheStuff -Trigger


Get-ScheduledTask -TaskPath "\" | foreach{
    Get-ScheduledTaskInfo $_.TaskName # display info such as LastRunTime, LastTaskResult, NextRunTime, NumberOfMissedRuns, TaskName, TaskPath, PSComputerName ... 
}
